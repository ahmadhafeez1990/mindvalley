package com.mindvalley.dataqueue.interfaces;


public interface RequestExecution {


    <T> void queue(Request<T> request, Callback<T> callback);
}
