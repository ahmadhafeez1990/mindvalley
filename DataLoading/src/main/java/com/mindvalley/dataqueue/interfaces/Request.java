package com.mindvalley.dataqueue.interfaces;

import com.mindvalley.dataqueue.implentations.Response;

import java.io.IOException;

public interface Request<T> {

    String getUrlHash();

    Response<T> execute() throws IOException;

    void queue(Callback<T> callback);

    void cancel();

    boolean isCancelled();



}
