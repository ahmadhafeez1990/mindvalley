package com.mindvalley.dataqueue.interfaces;

import com.mindvalley.dataqueue.implentations.Response;

public interface Callback<T> {

    public void onSuccess(Response<T> response);

    public void onError(Throwable t);
}
