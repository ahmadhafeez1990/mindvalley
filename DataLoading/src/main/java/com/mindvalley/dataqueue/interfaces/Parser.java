package com.mindvalley.dataqueue.interfaces;

import com.mindvalley.dataqueue.implentations.Response;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;


public interface Parser<T> {

    T fromBody(InputStream res, Type type) throws IOException;

    Response toBody(T obj);

}
