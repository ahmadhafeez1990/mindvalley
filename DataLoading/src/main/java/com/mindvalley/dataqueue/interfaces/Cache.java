package com.mindvalley.dataqueue.interfaces;



public interface Cache<T> {

    T put(String key, T value);

    T get(String key);

    T remove(String key);

    void clear();
}
