package com.mindvalley.dataqueue.implentations;

import com.mindvalley.dataqueue.interfaces.Cache;


public abstract class LruCache<T> extends android.util.LruCache<String, T> implements Cache<T> {

    public LruCache(int maxSize) {
        super(maxSize);
    }


    @Override
    protected T create(String key) {
        return super.create(key);
    }


    @Override
    public void clear() {
        super.evictAll();
    }

}
