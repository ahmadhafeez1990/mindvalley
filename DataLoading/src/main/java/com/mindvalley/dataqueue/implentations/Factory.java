package com.mindvalley.dataqueue.implentations;



import com.mindvalley.dataqueue.interfaces.Cache;
import com.mindvalley.dataqueue.interfaces.Parser;
import com.mindvalley.dataqueue.interfaces.Request;
import com.mindvalley.dataqueue.interfaces.RequestExecution;

import java.lang.reflect.Type;


public class Factory {
    private static final int CACHE_SIZE = 16 * 1024 * 1024;

    Parser parser;
    RequestExecution requestExecution;
    Cache cache;


    private Factory(Parser parser, RequestExecution requestExecution, Cache cache, String endpoint) {

        this.parser = parser;
        this.requestExecution = requestExecution;
        this.cache = cache;
    }

    public <T> Request<T> get(String url, Type resourceType) {
        RealRequest<T> req = null;


        try {
            req = new RealRequest<T>(url, parser, requestExecution, resourceType, cache );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return req;
    }


    public static class Builder {

        private Parser parser;
        private RequestExecution requestExecution = new RealRequestExecution();
        private String endpoint;
        private Cache cache;


        public Builder setParser(Parser parser) {
            this.parser = parser;
            return this;
        }

        public Builder setRequestExecution(RequestExecution requestExecution) {
            this.requestExecution = requestExecution;
            return this;
        }

        public Builder setCache(Cache cache) {
            this.cache = cache;
            return this;
        }

        public Builder setEndpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }


        public Factory createApiClient() {
            if(requestExecution == null || parser == null) throw new IllegalArgumentException();
            return new Factory( parser, requestExecution, cache, endpoint);
        }
    }


}
