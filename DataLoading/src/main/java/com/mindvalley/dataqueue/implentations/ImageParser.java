package com.mindvalley.dataqueue.implentations;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mindvalley.dataqueue.interfaces.Parser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

public class ImageParser implements Parser<Bitmap> {


    @Override
    public Bitmap fromBody(InputStream res, Type type) throws IOException {
       return BitmapFactory.decodeStream(res);
    }

    @Override
    public Response toBody(Bitmap obj) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        obj.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        InputStream is = new ByteArrayInputStream(stream.toByteArray());
        return null;
    }
}
