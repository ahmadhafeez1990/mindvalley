package com.mindvalley.dataqueue.implentations;

import android.os.Handler;

import com.mindvalley.dataqueue.interfaces.Callback;
import com.mindvalley.dataqueue.interfaces.RequestExecution;
import com.mindvalley.dataqueue.interfaces.Request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RealRequestExecution implements RequestExecution {

    final int CORE_SIZE = 2;
    final int MAX_SIZE = 5;
    private final Handler handler;

    ExecutorService service = Executors.newCachedThreadPool();

    HashMap<String, ArrayList<Callback>> listOfRequests = new HashMap<>();

    public RealRequestExecution() {
        this.handler = new Handler();
    }

    @Override
    public <T> void queue(final Request<T> request, final Callback<T> callback) {

        // Save callback and issue a resource id
        synchronized (listOfRequests) {
            if (listOfRequests.containsKey(request.getUrlHash())) {
                listOfRequests.get(request.getUrlHash()).add(callback);
            } else {
                ArrayList<Callback> callbacks = new ArrayList<>();
                callbacks.add(callback);
                listOfRequests.put(request.getUrlHash(), callbacks);
            }
        }

        service.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final Response<T> res = request.execute();
                    synchronized (listOfRequests) {
                        for (final Callback<T> rC : listOfRequests.get(request.getUrlHash())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    rC.onSuccess(res);
                                }
                            });
                        }
                    }

                } catch (final IOException e) {
                    synchronized (listOfRequests) {
                        for (final Callback<T> rC : listOfRequests.get(request.getUrlHash())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    rC.onError(e);
                                }
                            });
                        }
                    }
                }

            }
        });

    }
}

