package com.mindvalley.dataqueue.implentations;

import com.mindvalley.dataqueue.interfaces.Cache;
import com.mindvalley.dataqueue.interfaces.Callback;
import com.mindvalley.dataqueue.interfaces.RequestExecution;
import com.mindvalley.dataqueue.interfaces.Parser;
import com.mindvalley.dataqueue.interfaces.Request;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * Not a complete implementation doesnot cover POST, PUT, DELETE. Serves to demonstrate concept.
 */
public class RealRequest<T> implements Request<T>, Callback<T> {

    public static final int DEFAULT_TIMEOUT = 60 * 1000;
    public static final int READ_TIMEOUT = 60 * 1000;


    URL url;
    Parser<T> parser;
    RequestExecution requestExecution;
    Reader body;
    Type type;
    Cache cache;
    T responseBody;
    Callback<T> callback;

    private boolean isCancelled = false;

    public RealRequest(String url, Parser<T> parser, RequestExecution requestExecution, Type type, Cache cache) throws MalformedURLException {
        this( url, parser, requestExecution, null, type, cache);
    }

    public RealRequest( String url, Parser<T> parser, RequestExecution requestExecution, Reader body, Type type, Cache cache) throws MalformedURLException {

        this.url = new URL(url);
        this.parser = parser;
        this.requestExecution = requestExecution;
        this.body = body;
        this.type = type;
        this.cache = cache;

    }

    @Override
    public String getUrlHash() {
        return url.getHost() + url.getPath();
    }

    public Response<T> execute() throws IOException {

        if (isCancelled) return null;

        Response<T> res = new Response<>();
        HttpURLConnection conn = null;
        InputStream in = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setConnectTimeout(DEFAULT_TIMEOUT);
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setInstanceFollowRedirects(true);
            conn.connect();
            in = conn.getInputStream();
            responseBody = parser.fromBody(in, type);
            synchronized (cache) {
                if (cache.get(url.getPath()) ==null)
                    cache.put(url.getPath(), responseBody);
            }
            res.body = responseBody;

        }catch(Exception ex){
            ex.printStackTrace();

        } finally {
            if (conn != null) conn.disconnect();
        }

        return res;
    }

    public void queue(Callback<T> callback) {
        Response<T> res = new Response<>();
        Object o = null;
        synchronized (cache) {
            o = cache.get(url.getPath());
        }
        if (o != null) {
            res.setIsFromCache(true);
            res.body = (T) o;
                       callback.onSuccess(res);
            return;
        }
        this.callback = callback;
        requestExecution.queue(this, this);

    }

    @Override
    public void cancel() {
        synchronized (this) {
            isCancelled = true;
        }
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void onSuccess(Response<T> response) {
        if (!isCancelled) callback.onSuccess(response);
    }

    @Override
    public void onError(Throwable t) {
        if (!isCancelled) callback.onError(t);
    }
}
