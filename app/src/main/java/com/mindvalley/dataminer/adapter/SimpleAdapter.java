package com.mindvalley.dataminer.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.baoyz.widget.PullRefreshLayout;
import com.mindvalley.dataminer.R;
import com.mindvalley.dataminer.models.UrlList;
import com.mindvalley.dataminer.utility.Client;
import com.mindvalley.dataqueue.interfaces.Request;
import com.mindvalley.dataqueue.implentations.Response;
import com.mindvalley.dataqueue.interfaces.Callback;

import java.util.List;

/**
 * Created by Ahmad on 8/28/2016.
 */
public class SimpleAdapter extends ArrayAdapter<String> {


    List<UrlList> listOfUrls;
    PullRefreshLayout layout;
    public SimpleAdapter(Context context,List<UrlList> listOfUrls,PullRefreshLayout layout) {
        super(context, 0, new String[listOfUrls.size()]);
        this.listOfUrls = listOfUrls;
        this.layout=layout;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_list_view, parent, false);
        UrlList urlList  = listOfUrls.get(position);

        Request<Bitmap> req  = Client
                .getClientForImage()
                .get(urlList.getRegular(),null);
        layout.setRefreshing(true);
       final ImageView imageRaw = (ImageView)convertView.findViewById(R.id.imageRaw);
        req.queue(new Callback<Bitmap>() {
            @Override
            public void onSuccess(Response<Bitmap> response) {
                layout.setRefreshing(false);
                Bitmap b = response.body;
                setupImage(imageRaw,b);
            }

            @Override
            public void onError(Throwable t) {
                // TO-Do implementation of error.....

            }
        });
        return convertView;
    }
    void setupImage(ImageView view,Bitmap b){
         view.setImageBitmap(b);

    }
}