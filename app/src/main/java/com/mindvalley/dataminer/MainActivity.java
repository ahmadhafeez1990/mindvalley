package com.mindvalley.dataminer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.baoyz.widget.PullRefreshLayout;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mindvalley.dataminer.adapter.SimpleAdapter;
import com.mindvalley.dataminer.models.UrlList;
import com.mindvalley.dataminer.utility.RestClient;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements PullRefreshLayout.OnRefreshListener{

/*
*
* */

    private List<UrlList> listOfUrls = new ArrayList<UrlList>();
    @Bind(R.id.swipeRefreshLayout)
    PullRefreshLayout layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);//  instead of dagger butterknife is used since third party api integration testing is required.
        layout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        layout.setOnRefreshListener(this);
        loadImages(layout);


    }
    public void handleResponse(JSONArray response){
        //Preparing jsonUrls to lazyLoad them on later stages.

        /* for now in the client app i am just handling images
            But the library handle plain objects as well. for that just need to pass an object parser instead of image parser in the client factory that generates request.
        */

        for(int i = 0 ;i < response.length(); i++){
            try {
                JSONObject dataObj = response.getJSONObject(i);
                JSONObject data = dataObj.getJSONObject("urls"); // list of urls. /// fetching temporary urls just to show images.
                UrlList urlList = new UrlList();


                urlList.setRegular(data.getString("regular"));

                listOfUrls.add(i,urlList);
                // To-DO override this function to handle xml as well///
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
    public void loadImages( final PullRefreshLayout layout){

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                handleResponse(response);
                SimpleAdapter sAdap = new SimpleAdapter(getApplicationContext(),listOfUrls,layout);

                ListView productListView = (ListView)findViewById(R.id.productListView);
                productListView.setAdapter(sAdap);


            }
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable e, JSONObject errorResponse) {
                // TO-DO onFailureHandle();
            }
        };
        try {

            //A simple rest client that handles both set and get. i've made it in past along with my properties file that setup application_properties
            RestClient.get(this, "/wgkJgazE/", null, handler);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void onRefresh() {
        loadImages(layout);
    }
}
