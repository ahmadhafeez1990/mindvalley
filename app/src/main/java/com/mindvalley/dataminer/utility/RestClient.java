package com.mindvalley.dataminer.utility;

/**
 * Created by alirizwan on 10/08/15.
 */

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

public class RestClient {

    private static String BASE_URL="http://pastebin.com/raw";


    public static AsyncHttpClient client = new AsyncHttpClient();


    public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) throws JSONException {

        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) throws JSONException {

        client.post(getAbsoluteUrl(url), params, responseHandler);

    }

    public static void post(Context context, String url, HttpEntity entity, String contentType, AsyncHttpResponseHandler responseHandler)throws JSONException {
        client.post(context, getAbsoluteUrl(url), entity, contentType, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {

        return BASE_URL + relativeUrl;
    }


}