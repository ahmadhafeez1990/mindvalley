package com.mindvalley.dataminer.utility;

import com.mindvalley.dataqueue.implentations.Factory;
import com.mindvalley.dataqueue.implentations.ImageCache;
import com.mindvalley.dataqueue.implentations.ImageParser;
import com.mindvalley.dataqueue.implentations.RealRequestExecution;

/**
 * Created by Ahmad on 8/28/2016.
 */
public class Client {
    private static Factory imgFactory = null;
    private static RealRequestExecution re = new RealRequestExecution();

    public static Factory getClientForImage() {
        if (imgFactory != null) return imgFactory;
        imgFactory = new Factory.Builder() .setParser(new ImageParser())
                .setCache(new ImageCache(8*1024*1024))
                .setRequestExecution(re)
                .createApiClient();
        return imgFactory;
    }

    /// an xml parser can be written in order to parse simple xml incase of image a new parser and cache is required only.

}
